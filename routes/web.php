<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::post('/login', 'Auth\LoginController@login');
Route::get( '/logout',  'Auth\LoginController@logout'  );
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/dashboard/create', 'DashboardController@create');
Route::get('/dashboard/{id}', 'DashboardController@show');
Route::post('/dashboard', 'DashboardController@save');
Route::get( '/dashboard/delete/{id}',  'DashboardController@delete'  );
