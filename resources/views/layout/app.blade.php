<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      
    <link id="" rel="shortcut icon" href="/images/favicon.ico?" />


    <!-- Google Font face -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i|Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <!-- Bootstrap -->
    

    <script>
        window.auth_user  = {!! json_encode(Auth::check()); !!};
    </script>
</head>



<div id="app">
    <div class="loading-holder">
        <div id="loading"></div>
    </div>
 
  @yield('content')


 </div>
<script src="https://code.jquery.com/jquery.min.js"></script>

<script src="{{ asset('js/app.js')}}"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>




</html>
