require('./bootstrap');
import store from './store.js';
import VueSweetalert2 from 'vue-sweetalert2';

Vue.component('login', require('./components/User/Login.vue').default);
Vue.component('dashboard', require('./components/Dashboard/list.vue').default);
Vue.component('create-branches', require('./components/Dashboard/create.vue').default);
Vue.component('edit-branches', require('./components/Dashboard/edit.vue').default);




new Vue({
    el: '#app',
    store: new Vuex.Store(store),
    strict: true,
});
Vue.use(VueSweetalert2);
