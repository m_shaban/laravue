<header class="clearfix">
    <div class="full-width">
        <div class="container">
            <div class="row nav-section">
                <div class="col-lg-1 col-sm-2 clearfix">
                   
                </div><!-- /.col-lg-10 -->
                <div class="col-lg-6 col-sm-12 clearfix">
                    <div class="navigation full-width">
                        <nav class="navbar navbar-default">
                            <div class="navbar-header">
                                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse js-navbar-collapse">
                                <ul class="nav navbar-nav">
                                    <li>
                                        <a href="{{ route('pageDetails','about-us') }}">{{__('website.about_us')}}</a>
                                    </li><!--/li-->
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{__('website.categories_label')}} <span class="caret"></span></a>
                                        <ul class="dropdown-menu">

                                            
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ route('website.product.offers') }}">{{__('website.special_offers_label')}}</a>
                                    </li><!--/li-->
                                    <li>
                                        <a href="{{ route('contactUs') }}">{{__('website.contact_us')}}</a>
                                    </li><!--/li-->
                                </ul>
                            </div>
                            <!-- /.nav-collapse -->

                            <!-- /.search_box_cover -->
                        </nav>
                        <!-- /.navigation -->
                    </div>
                    <!-- /.col-lg-10 -->
                </div><!-- /.col-lg-11 -->
                <div class="col-lg-5 col-sm-12 top-sec clearfix">
                    <ul class="top-links">
                        <li>
                            <a class="main" href="{{ route('website.register') }}">{{__('website.Register_label')}}</a>
                        </li><!--/li-->
                      
                      
                       
                        <li>
                            <a class="main" href="#" id="cartEnable"><img src="/images/cart.png" alt=""><span class="hidden-xs">
                        <cart-count /></span></a>
                            <div class="cart_box" id="cart_bx">
                                @include('includes.cart')
                                <!-- /.cart_contents -->
                            </div><!--/.cart_box-->
                        </li><!--/li-->
                        <li>
                            <a class="main" href="#search">
                                <span class=""><img src="/images/search.png" alt=""></span>
                            </a>
                        </li><!--/li-->
                        <li>
                            <a href="{{ route('switch_lang' , [$lang == 'en' ? 'ar' : 'en'])}}"  class="main"
                            >
                               
                                @if( $lang == 'en')
                                    <img src="/images/ar.png" alt="">
                                    @else
                                    <img src="/images/lang-eng.png" alt="">
                                    @endif



                            </a>

                        </li><!--/li-->
                    </ul>
                </div>
            </div><!-- /.row -->
        </div><!--/.container-->
    </div>

    <!--Seaarch Pop up-->
   </header><!--/header-->

<body>


