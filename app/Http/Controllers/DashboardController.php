<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Auth;
use Illuminate\Support\Str;
use App\Models\Branches;


class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $branches = Branches::where('user_id',Auth::Id())->get();   
        
        return view('dashboard.view')->with('branches',$branches);
        
    }
    public function create(Request $request)
    {
        return view('dashboard.create');
    }
    public function show(Request $request)
    {
        $branch = Branches::find($request->id);
        return view('dashboard.edit')->with('branch',$branch);
    }
    public function save(Request $request)
    {
        $branch = Branches::updateOrCreate(
            ['id' =>$request->id],
            ['name' => $request->name, 'description' => $request->description
                ,'user_id'=>Auth::Id()]
        );
        return json_encode(['status'=>'true']);
    }
    public function delete(Request $request)
    {
        $branch = Branches::whereId($request->id)->delete();
        return redirect(route('dashboard'));
    }
}