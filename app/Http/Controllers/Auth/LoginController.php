<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
  public function login(Request $request)
    {

        $chkemail = User::where('email', $request->email)->first();

        if (!$chkemail) {
            return response()->json(['logged' => false, 'code' => 1, 'message' => 'Invalid Email']); // 
        } 
        $credentials = array(
            'email' => $request->email,
            'password' => ($request->password)
        );
         if (Auth::attempt($credentials)) {
             return response()->json(['logged' => true,'user'=>Auth::user()]);
        } else {
            return response()->json(['logged' => false, 'code' => 2, 'message' => 'Wrong Password']); //            return redirect()->rout('customer.login')->withErrors('Invalid Login credentials');
        }
    }
    public function logout()
    {
        Auth::logout();
             return response()->json(['loggedout' => true]);

    }
}
